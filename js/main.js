let student_list = JSON.parse(localStorage.getItem("student_list")) || [];
let new_student_list = [];
if (student_list.length) {
  new_student_list = student_list.map((item) => {
    return new SinhVien(
      item._id,
      item._name,
      item._pass,
      item._email,
      item._math,
      item._physics,
      item._chemistry
    );
  });

  for (var i = 0; i < student_list.length; i++) {
    let current_item = student_list[i];
    student_list[i] = new SinhVien(
      current_item._id,
      current_item._name,
      current_item._pass,
      current_item._email,
      current_item._math,
      current_item._physics,
      current_item._chemistry
    );
  }
  displayStudentList(student_list);
}

let addSV = () => {
  let student_obj = getFormValue();
  // validate duplicated id
  const f = student_list.find(item => item._id === student_obj._id)
  if (validateForm(student_obj) && !f) {
    student_list.push(student_obj);
    storeStudentList(student_list);
    displayStudentList(student_list);
  }
  const eid = document.getElementById("spanMaSV");
  if (f) {
    if (eid.innerText.length === 0) {
      eid.innerText = 'Trùng ID';
    } else {
      eid.innerText = '';
    }
  }
};

let xoaSV = (id) => {
  handleButtons(1);
  student_list.splice(
    student_list.findIndex((obj) => obj._id === id),
    1
  );
  storeStudentList(student_list);
  displayStudentList(student_list);
};

let suaSV = (id) => {
  let student_index = student_list.findIndex((obj) => obj._id === id);
  if (student_index > -1) {
    let student_obj = student_list[student_index];
    prepareFormToEdit(student_obj);
    const cancelBtn = document.getElementById("cancelUpdate");
    document.getElementById("txtMaSV").setAttribute("disabled", "");

    cancelBtn.removeAttribute("hidden");
    document.getElementById("updateTable").addEventListener("click", () => {
      let new_student_obj = getFormValue();
      student_list[student_index] = new_student_obj;
      cancelBtn.setAttribute("hidden", "");
      storeStudentList(student_list);
      displayStudentList(student_list);
      document.getElementById("txtMaSV").removeAttribute("disabled");
      document.getElementById('formQLSV').reset()

    });
  }
};

let searchSV = (ar, text) => {
  if (text.length) {
    const filtered = ar.filter(item => item._id === text || item._name.includes(text));
    displayStudentList(filtered);
  } else {
    displayStudentList(ar)
  }
}

document.querySelectorAll("#addSV")[0].addEventListener("click", () => {
  addSV();
});

document.getElementById('btnSearch').addEventListener("click", () => {
  const textToSearch = document.getElementById('txtSearch').value;
  searchSV(student_list, textToSearch);
});
document.getElementById('clearTable').addEventListener("click", () => {
  localStorage.removeItem('student_list')
  student_list = [];
  displayStudentList([])
});
document.getElementById('cancelUpdate').addEventListener("click", () => {
  document.getElementById("cancelUpdate").setAttribute("hidden", "");
  document.getElementById("txtMaSV").removeAttribute("disabled");
  document.getElementById('formQLSV').reset()
});