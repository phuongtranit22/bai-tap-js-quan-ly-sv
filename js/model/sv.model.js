function SinhVien(_id, _name, _pass, _email, _math, _physics, _chemistry) {
  this._id = _id;
  this._name = _name;
  this._pass = _pass;
  this._email = _email;
  this._math = _math;
  this._physics = _physics;
  this._chemistry = _chemistry;
}

SinhVien.prototype.getAvg = function () {
  let math_point = parseFloat(this._math * 1) || 0;
  let physics_point = parseFloat(this._physics * 1) || 0;
  let chemistry_point = parseFloat(this._chemistry * 1) || 0;
  return parseFloat((math_point + physics_point + chemistry_point) / 3).toFixed(2);
};
