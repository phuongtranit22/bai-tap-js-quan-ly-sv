function Validator(value, target) {
  this._value = value;
  this._target = target;
  this._message = "";
  this.isEmtpy = isEmtpy;
  this.is_valid_email = is_valid_email;

  const REGEX_EMAIL = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  function setErrorMessage(msg) {
    this._message = msg;
  }

  function getErrorMessage() {
    return this._message;
  }
  function isEmtpy(msg) {
    setErrorMessage(msg);
    if (this._value.length) {
      setErrorMessage("");
    }
    document.querySelectorAll(this._target)[0].innerText = getErrorMessage();
    return this._value.length;
  }
  function is_valid_email(msg) {
    setErrorMessage(msg);
    REGEX_EMAIL.test(this._value) && setErrorMessage("");
    document.querySelectorAll(this._target)[0].innerText = getErrorMessage();
    return REGEX_EMAIL.test(this._value);
  }
}
