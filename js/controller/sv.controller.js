let getFormValue = () => {
  const id = document.getElementById("txtMaSV").value;
  const name = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const pass = document.getElementById("txtPass").value;
  const math = document.getElementById("txtDiemToan").value;
  const physics = document.getElementById("txtDiemLy").value;
  const chemistry = document.getElementById("txtDiemHoa").value;

  return new SinhVien(id, name, pass, email, math, physics, chemistry);
};

let prepareFormToEdit = (std) => {
  document.getElementById("txtMaSV").value = std._id;
  document.getElementById("txtTenSV").value = std._name;
  document.getElementById("txtEmail").value = std._email;
  document.getElementById("txtPass").value = std._pass;
  document.getElementById("txtDiemToan").value = std._math;
  document.getElementById("txtDiemLy").value = std._physics;
  document.getElementById("txtDiemHoa").value = std._chemistry;
};

let deleteHandler = (status) => {
  document.querySelectorAll(".student .action .icon-del").forEach((btn) => {
    if (status === 0) {
      btn.addEventListener("click", () => {
        xoaSV(btn.dataset.target);
      });
    }

    if (status === 1) {
      btn.removeEventListener("click", () => {
        xoaSV(btn.dataset.target);
      });
    }
  });
};

let editHandler = (status) => {
  document.querySelectorAll(".student .action .icon-edit").forEach((btn) => {
    if (status === 0) {
      btn.addEventListener("click", () => {
        suaSV(btn.dataset.target);
      });
    }

    if (status === 1) {
      btn.removeEventListener("click", () => {
        suaSV(btn.dataset.target);
      });
    }
  });
};

let handleButtons = (status) => {
  deleteHandler(status);
  editHandler(status);
};

let validateForm = (obj) => {
  validateId(obj._id, "#spanMaSV");
  validateName(obj._name, "#spanTenSV");
  validateEmail(obj._email, "#spanEmailSV");
  validatePass(obj._pass, "#spanMatKhau");
  validateScoreMath(obj._math, "#spanToan");
  validateScorePhysics(obj._physics, "#spanLy");
  validateScoreChemistry(obj._chemistry, "#spanHoa");
  const isValid = [
    document.getElementById("spanMaSV"),
    document.getElementById("spanTenSV"),
    document.getElementById("spanEmailSV"),
    document.getElementById("spanMatKhau"),
    document.getElementById("spanToan"),
    document.getElementById("spanLy"),
    document.getElementById("spanHoa"),
  ].some(item => item.innerText.length === 0)
  return isValid;
};

let validateId = (_id, _target) => {
  let v = new Validator(_id, _target);
  return v.isEmtpy("Bắt buộc nhập");
};

let validateName = (_name, _target) => {
  return new Validator(_name, _target).isEmtpy(
    "Bắt buộc nhập"
  );
};

let validateEmail = (_email, _target) => {
  let v = new Validator(_email.toLowerCase(), _target);
  return (
    v.isEmtpy("Bắt buộc nhập") &&
    v.is_valid_email("Sai format")
  );
};

let validatePass = (_pass, _target) => {
  let v = new Validator(_pass, _target);
  return v.isEmtpy("Bắt buộc nhập");
};

let validateScoreMath = (_math, _target) => {
  let v = new Validator(_math, _target);
  return v.isEmtpy("Bắt buộc nhập");
};

let validateScorePhysics = (_physics, _target) => {
  let v = new Validator(_physics, _target);
  return v.isEmtpy("Bắt buộc nhập");
};

let validateScoreChemistry = (_chemistry, _target) => {
  let v = new Validator(_chemistry, _target);
  return v.isEmtpy(
    "Bắt buộc nhập"
  );
};


let storeStudentList = (data) => {
  localStorage.setItem("student_list", JSON.stringify(data));
};

let displayStudentList = (student_list) => {
  const el = document.getElementById("studentTable").querySelector('tbody');
  el.innerHTML = "";
  student_list.forEach((student) => {
    let h = ``;
    h = `
        <tr class="student student-${student._id}">
            <td>${student._id}</td>
            <td>${student._name}</td>
            <td>${student._email}</td>
            <td align="right">${student.getAvg()}</td>
            <td class="action" width="120px" align="right">
              <i class="icon icon-edit fa-solid fa-pen-to-square" data-target="${student._id}"></i>
              <i class="icon icon-del fa-solid fa-xmark" data-target="${student._id}"></i>
            </td>
        </tr>
    `;
    el.innerHTML += h;
  });
  handleButtons(0);
};


